<?php

namespace App\Rules;

use App\Classes\PredictionValidation;
use Illuminate\Contracts\Validation\Rule;

class PredictionRule implements Rule
{

    protected $type;

    /**
     * Create a new rule instance.
     *
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $prediction = new PredictionValidation();
        return $prediction->validate($value, $this->type);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Prediction format is not correct.';
    }
}
