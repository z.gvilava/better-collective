<?php

namespace App\Repositories\Event;

use App\Classes\PredictionValidation;
use App\Models\Event;
use App\Repositories\Event\EventRepositoryInterface;

class EventRepository implements EventRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all(): object
    {
        return Event::all();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): object
    {
        $prediction = new PredictionValidation();
        return Event::create(array_merge($data, ['status' => $prediction->setResult($data['prediction'])]));
    }

    /**
     * @inheritDoc
     */
    public function show(int $id): object
    {
        return Event::findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data): bool
    {
        return Event::findOrFail($id)->update($data);
    }

    /**
     * @inheritDoc
     */
    public function destroy(int $id): bool
    {
        return Event::destroy($id);
    }
}

