<?php

namespace App\Repositories\Prediction;

interface PredictionRepositoryInterface
{
    /**
     * Display a listing of the resource.
     *
     * @return object
     */
    public function all(): object;

    /**
     * Store a newly created resource in storage.
     *
     * @param array $data
     * @return object
     */
    public function create(array $data): object;

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return object
     */
    public function show(int $id): object;

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data): bool;

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return bool
     */
    public function destroy(int $id): bool;
}
