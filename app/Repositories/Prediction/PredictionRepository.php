<?php

namespace App\Repositories\Prediction;

use App\Classes\PredictionValidation;
use App\Models\Prediction;

class PredictionRepository implements PredictionRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all(): object
    {
        return Prediction::orderBy('id', 'desc')->get();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): object
    {
        $prediction = new PredictionValidation();
        return Prediction::create(array_merge($data, ['status' => $prediction->setResult($data['prediction'])]));
    }

    /**
     * @inheritDoc
     */
    public function show(int $id): object
    {
        return Prediction::findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data): bool
    {
        return Prediction::findOrFail($id)->update($data);
    }

    /**
     * @inheritDoc
     */
    public function destroy(int $id): bool
    {
        return Prediction::destroy($id);
    }
}

