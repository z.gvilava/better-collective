<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\PredictionResource;

class PredictionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return object
     */
    public function toArray($request): object
    {
        return PredictionResource::collection($this->collection);
    }
}
