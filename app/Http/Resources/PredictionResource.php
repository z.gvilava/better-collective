<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int id
 * @property int event_id
 * @property string market_type
 * @property string prediction
 * @property string status
 * @property object events
 */
class PredictionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'event_id' => $this->event_id,
            'event_name' => $this->events->name,
            'market_type' => $this->market_type,
            'prediction' => $this->prediction,
            'status' => $this->status
        ];
    }
}
