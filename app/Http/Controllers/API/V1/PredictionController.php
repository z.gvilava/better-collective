<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\PredictionStoreRequest as StoreRequest;
use App\Http\Requests\PredictionUpdateRequest as UpdateRequest;
use App\Repositories\Prediction\PredictionRepository as Repository;
use App\Http\Resources\PredictionResource as Resource;
use App\Http\Resources\PredictionCollection as Collection;

class PredictionController extends Controller
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()
            ->json(new Collection($this->repository->all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        return response()
            ->json(new Resource($this->repository->create($request->all())), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, int $id): JsonResponse
    {
        return response()
            ->json($this->repository->update($id, $request->all()), 204);
    }
}
