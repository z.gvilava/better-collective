<?php

namespace App\Http\Requests;

use App\Rules\PredictionRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * @property string market_type
 */
class PredictionStoreRequest extends FormRequest
{

    /**
     * Returns json errors for REST API
     *
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'status' => false,
                'messages' => $validator->errors()->all()
            ], 400)
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_id' => 'required|integer|exists:events,id',
            'market_type' => 'required|in:1x2,correct_score',
            'prediction' => ['required', 'string', new PredictionRule($this->market_type)]
        ];
    }
}
