<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prediction extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id',
        'market_type',
        'prediction',
        'status'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'event_id' => 'integer',
        'market_type' => 'string',
        'prediction' => 'string',
        'status' => 'string'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['events'];

    /**
     * Get the event for predictions.
     */
    public function events()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');
    }

}
