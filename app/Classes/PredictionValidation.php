<?php

namespace App\Classes;

class PredictionValidation
{

    /**
     * Validate prediction
     *
     * @param string $prediction
     * @param string $type
     * @return bool
     */
    public function validate(string $prediction, string $type): bool
    {
        if ($type === "correct_score" && $this->scoreFormatValidator($prediction))
            return true;
        elseif ($type === "1x2" && $this->predictionFormatValidator($prediction))
            return true;
        else return false;
    }


    public function setResult(string $result): string
    {
        switch ($result) {
            case "1":
                return "win";
                break;
            case "x":
                return "unresolved";
                break;
            case "2":
                return "lost";
                break;
            default:
                return $this->setResultFromScore($result);
        }
    }

    private function setResultFromScore(string $result): string
    {
        $score = explode(':', $result);

        if ($score[0] > $score[1])
            return "win";
        else if ($score[0] < $score[1])
            return "lost";
        else return "unresolved";
    }

    /**
     * Validate score format
     * @param string $score
     * @return bool
     */
    private function scoreFormatValidator(string $score): bool
    {
        $reg = '/^[0-9]+:[0-9]+$/';
        if (preg_match($reg, $score))
            return true;
        else return false;
    }

    /**
     * Validate prediction format
     *
     * @param string $prediction
     * @return bool
     */
    private function predictionFormatValidator(string $prediction): bool
    {
        $reg = '/[1x2]/';
        if (strlen($prediction) === 1 && preg_match($reg, $prediction))
            return true;
        else return false;
    }

}
