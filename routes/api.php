<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {

    Route::get('events', ['as' => 'events.index', 'uses' => 'EventController@index']);

    Route::get('predictions', ['as' => 'predictions.index', 'uses' => 'PredictionController@index']);
    Route::post('predictions', ['as' => 'predictions.store', 'uses' => 'PredictionController@store']);
    Route::put('predictions/{id}/status', ['as' => 'predictions.status.update', 'uses' => 'PredictionController@update']);

});


