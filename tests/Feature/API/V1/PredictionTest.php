<?php

namespace Tests\Feature\API\V1;

use App\Models\Prediction;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PredictionTest extends TestCase
{

    use DatabaseTransactions;

    public function testIndex(){

        factory(\App\Models\Prediction::class, 1)->create();

        $response =  $this->json('GET', route('predictions.index'));

        $response->assertStatus(200)
            ->assertJson([]);

    }

    public function testStore(){

        $data = [
            'event_id' => 1,
            'market_type' => '1x2',
            'prediction' => '1/2',
            'status' => 'lost',
        ];

        $response =  $this->json('POST', route('predictions.index'), $data);

        $response->assertStatus(201)
            ->assertJson([
                'event_id' => 1,
                'market_type' => '1x2',
                'prediction' => '1/2',
                'status' => 'lost'
            ]);

    }

    public function testUpdate(){

        $data = [
            'event_id' => 1,
            'market_type' => '1x2',
            'prediction' => '1/2',
            'status' => 'lost',
        ];

        $model = Prediction::create($data);

        $response =  $this->json('PUT', route('predictions.status.update', $model->id), [
            'status' => 'win',
        ]);

        $response->assertStatus(204);

    }
}
