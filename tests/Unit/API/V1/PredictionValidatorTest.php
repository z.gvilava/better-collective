<?php

namespace Tests\Unit\API\V1;

use App\Classes\PredictionValidation;
use PHPUnit\Framework\TestCase;

class PredictionValidatorTest extends TestCase
{
    /**
     * Check if PredictionValidation returns true when WIN.
     *
     * @return void
     */
    public function testCorrectScoreWinTrue()
    {
        $result = new PredictionValidation();

        $this->assertTrue($result->validate('5:2', 'correct_score'));
    }

    /**
     * Check if PredictionValidation returns true when LOST.
     *
     * @return void
     */
    public function testCorrectScoreLostTrue()
    {
        $result = new PredictionValidation();

        $this->assertTrue($result->validate('2:5', 'correct_score'));
    }

    /**
     * Check if PredictionValidation returns true when DRAW.
     *
     * @return void
     */
    public function testCorrectScoreDrawTrue()
    {
        $result = new PredictionValidation();

        $this->assertTrue($result->validate('5:5', 'correct_score'));
    }

    /**
     * Check if PredictionValidation returns false when incorrect data.
     *
     * @return void
     */
    public function testCorrectScoreIncorrectDataFalse()
    {
        $result = new PredictionValidation();

        $this->assertFalse($result->validate('5:5', '1x2'));
    }

}
