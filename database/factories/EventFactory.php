<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {

    $events = [
        "Dortmund - Paris SG",
        "Real Madrid - Manchester City",
        "Atlanta - Valencia",
        "Atl. Madrid - Liverpool",
        "Chelsea - Bayern Munich",
        "Lyon - Juventus",
        "Tottenham - RB Leipzig",
        "Napoli - Barcelona",
    ];

    return [
        'name' => $faker->unique()->randomElement($events)
    ];
});
